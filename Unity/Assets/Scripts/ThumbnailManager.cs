﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager : MonoBehaviour
{

    public Transform container; // container that stores all rows and padding
    public GameObject thumbnailPrefab;
    public GameObject rowPrefab;
    public GridLayoutGroup padTop;
    public GridLayoutGroup padBottom;

    private int _maxInitialRows = 16;
    private int _thumbnailsPerRow = 5;
    private int _totalThumbs = 1000;
    private float _rowHeight = 216f; // 172 thumbnail + 44 padding
    private float _thumbnailHeight = 172f;

    /// <summary>
    /// Lowest index of currently created thumbnails
    /// </summary>
    private int _currentMinThumbIndex = 0;

    /// <summary>
    /// Highest index of currently created thumbnails
    /// </summary>
    private int _currentMaxThumbIndex = 0;

    private int _createdThumbs = 0;
    private int _rowsScrolled = 0;
    private int _rowsScrolledPrev = 0;
    private int _buildRowDelay = 1; // wait this amount of rows before building
    private int _buildRowLower; // lower limit of cycle window
    private int _buildRowUpper; // upper limit of cycle window
    private float _totalHeight;
    private int _totalRows;

    private float _yPadTop = 0f;
    private float _yPadBottom = 0f;
    
    private int _firstIndex; // sibling  index of pad top 
    private int _lastIndex; // sibling index of pad bottom

    private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
    private RectTransform _outerContainerRect; // container that stores all rows and padding
    

    void Start()
    {
        Application.targetFrameRate = 60;

        _outerContainerRect = container.GetComponent<RectTransform>();

        CreateThumbnailVOList();
        CreateThumbnailPrefabs();
    }


    // ____ creation ____
    //---------------------

    /// <summary>
    /// Create thumbnail data
    /// </summary>
    private void CreateThumbnailVOList()
    {
        ThumbnailVO thumbnailVO;
        for (int i = 0; i < _totalThumbs; i++)
        {
            thumbnailVO = new ThumbnailVO();
            thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
    }


    /// <summary>
    /// Create visible rows of thumbnails and calculate space needed
    /// We will cycle off-screen rows as needed, and pad top and bottom for smooth scrolling
    /// </summary>
    private void CreateThumbnailPrefabs()
    {

        GameObject gameObj;

        // build initial visisble rows of thumbnails
        for (int i = 0; i < _maxInitialRows; i++)
        {
            GameObject row = AddNewRow();
            for (int j = 0; j < _thumbnailsPerRow; j++)
            {
                gameObj = (GameObject)Instantiate(thumbnailPrefab);
                gameObj.transform.SetParent(row.transform, false);
                gameObj.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[_createdThumbs];

                _createdThumbs++;
            }
        }

        _currentMaxThumbIndex = _createdThumbs -1; // store this to work out min/max thumbs later

        //set initial bottom padding size
        _totalRows = (_totalThumbs / _thumbnailsPerRow);
        _totalHeight = _rowHeight * _totalRows;
        _yPadBottom = _totalHeight - (_rowHeight * _maxInitialRows);
        padBottom.cellSize = new Vector2(10, _yPadBottom);
        padBottom.transform.SetAsLastSibling(); // move back to bottom in hierarchy now that rest have been created

        // store the sibling indices for when we are cycling rows before and after these
        _firstIndex = padTop.transform.GetSiblingIndex();
        _lastIndex = padBottom.transform.GetSiblingIndex();
        
        //set a nice window for cycling
        _buildRowUpper = _buildRowDelay;
        _buildRowLower = _totalRows - Mathf.CeilToInt(container.GetComponent<RectTransform>().rect.height / _rowHeight) - _buildRowDelay;
    }


    /// <summary>
    /// Create a row container for thumbnails
    /// </summary>
    private GameObject AddNewRow()
    {
        GameObject row = (GameObject)Instantiate(rowPrefab);
        row.transform.SetParent(container, false);
        return row;
    }


    // _________ cycle _________
    //-----------------------------

    /// <summary>
    /// Called when a scroll movement happens, adds/removes thumbnail rows as required
    /// </summary>
    public void ScrollPosChanged(Vector2 value)
    {
        // count the number of rows we have scrolled past
        _rowsScrolled = Mathf.FloorToInt((_outerContainerRect.localPosition.y) / _rowHeight); // 172 space + 44 padding

        // if scrolling down (rows scrolled increasing), and inside cycle window
        if ((_rowsScrolled > _rowsScrolledPrev) && (_rowsScrolledPrev > _buildRowUpper && _rowsScrolledPrev < _buildRowLower))
        {
            // if we reach the end we don't want to cycle
            if (_currentMaxThumbIndex >= _totalThumbs - 1)
            {
                _currentMaxThumbIndex = _totalThumbs - 1;
                _currentMinThumbIndex = _totalThumbs - _createdThumbs;
            }
            else
            {
                // for each row that we have scrolled, cycle rows as needed, and modify thumbnail content
                for (int i = 0; i < (_rowsScrolled - _rowsScrolledPrev); i++)
                {
                    if (_currentMaxThumbIndex + 1 >= _totalThumbs) { break; } // don't continue past thumbnails that we have

                    // update the thumbnails, and cycle to upcoming position
                    modifyThumbnailRow(container.GetChild(_firstIndex + 1), _currentMaxThumbIndex + 1);
                    container.GetChild(_firstIndex + 1).SetSiblingIndex(_lastIndex - 1);

                    // update min and max positions
                    UpdateThumbnailPositions(_thumbnailsPerRow);

                    // modify padding
                    UpdatePadding(_rowHeight, -_rowHeight);
                }
            }
        }
        // if scrolling up (rows scrolled decreasing), and inside cycle window
        else if ((_rowsScrolled < _rowsScrolledPrev) && (_rowsScrolledPrev > _buildRowUpper - 1 && _rowsScrolledPrev < _buildRowLower - 1))
        {
            // don't cycle if we are at the start
            if (_currentMinThumbIndex < 0)
            {
                _currentMinThumbIndex = 0;
                _currentMaxThumbIndex = _createdThumbs - 1;
            }
            else
            {
                // for each row that we have scrolled, cycle rows as needed, and modify thumbnail content
                for (int i = 0; i < (_rowsScrolledPrev - _rowsScrolled); i++)
                {
                    if (_currentMinThumbIndex - _thumbnailsPerRow < 0) { break; } // don't continue past thumbnails that we have

                    // update the thumbnails, and cycle to upcoming position
                    modifyThumbnailRow(container.GetChild(_lastIndex - 1), _currentMinThumbIndex - _thumbnailsPerRow);
                    container.GetChild(_lastIndex - 1).SetSiblingIndex(_firstIndex + 1);

                    // update min and max positions
                    UpdateThumbnailPositions(-_thumbnailsPerRow);

                    // modify padding
                    UpdatePadding(-_rowHeight, _rowHeight);
                }
            }
        }

        _rowsScrolledPrev = _rowsScrolled;
    }


    /// <summary>
    /// Modify the current info that a thumbnail row is showing (image and text)
    /// </summary>
    private void modifyThumbnailRow(Transform t, int rowStartIndex)
    {
        foreach (Thumbnail thumb in t.GetComponentsInChildren<Thumbnail>())
        {
            thumb.thumbnailVO = _thumbnailVOList[rowStartIndex]; //
            rowStartIndex++;
        }
    }


    /// <summary>
    /// Update the thumbnail min and max index values
    /// </summary>
    private void UpdateThumbnailPositions(int change)
    {
        _currentMaxThumbIndex += change;
        _currentMinThumbIndex += change;
    }


    /// <summary>
    /// Change the amount of padding at top and bottom
    /// </summary>
    private void UpdatePadding(float topChange, float bottomChange)
    {
        _yPadTop += topChange;
        _yPadBottom += bottomChange;
        if (_yPadTop < 0) { _yPadTop = 0; }
        if (_yPadBottom < 0) { _yPadBottom = 0; }
        padTop.cellSize = new Vector2(10, _yPadTop);
        padBottom.cellSize = new Vector2(10, _yPadBottom);
    }

}
